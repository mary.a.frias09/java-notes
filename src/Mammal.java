//abstract class Mammal {
//    public Mammal() {
//
//    }
////Abstract classes cannot be instantiated the below code will return an error
////Mammal m1 = new Mammal();
//
//    public void displayInfo(){
//        System.out.println("I am a mammal. ");
//    }
//
//
//}
//
// class Dolphin extends Mammal {
//
//void Mammal(){
//    super();
//}
//
//}
//
//class Main {
//    public static void main(String[] args) {
//        Dolphin d1 = new Dolphin();
//
//        d1.displayInfo();
//    }
//}


abstract class Mammal {

    public Mammal() {
    }
}


class Dolphin extends Mammal {
    public Dolphin() {

        super();
    }
}

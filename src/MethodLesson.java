public class MethodLesson {
    public static void main(String[] args) {

        //METHODS - are a sequence of statements that perform a specific task.

//        //CALL MY greeting() method
//        greeting("Hello", "bravo");
//
//        System.out.println(returnFive());
//
//        System.out.println(yelling("Hello World"));
//        System.out.println(message());
//        System.out.println(message("Newsletter" , "REPLIED"));
////
//        String changeMe = "Hello Bravo!";
//        String secondString = "Hello CodeBound";
//         changeString(secondString);
////
//        System.out.println(secondString);
////
//        counting(5);



    }///end of Main Method, don't delete




    //basic SYNTAX for a method:
//    public static returnType(param1,param2){
//        we want the code to do.
//    }
//                       greeting = name of method
//    public static String greeting(String name) {
//        System.out.println();
//        return String.format("Hello %s!", name);
//    }

//    public static void greeting(String greet, String name) {
//        System.out.printf("%s, %s!\n", greet, name);
//    }

//    public static int returnFive() {
//        return 5;
//    }

//    public static String yelling(String s) {
//        return s.toUpperCase();
//    }




    // public - this is the visibility modifier
    // defines whether or not other classes can "see" this method.

    // static - the presence of this keyword defines that the method
    // belongs to the class, as opposed to instance of it.

    // METHOD OVERLOADING
    // - defining multiple methods with the same name but with different parameter type,
    // parameter order, or number of parameters

    public static String message () {
        return "This is an example of methods";
    }

    public static String message(String memo) {
        return memo;
    }

    public static String message(int code) {
        return "Code :" + code + " message";
    }

    public static String message(String memo, String status) {
        return memo + " Status: " + status;
    }

//      PASSING PARAMETERS TO METHODS
    public static void changeString(String s) {
        s = "This is a string";
    }

    // RECURSION = that aims to solve a problem by dividing it into small chunks

    // counting 5 to 1 using recursion
//    public static void counting(int num) {
//        if (num <= 0) {
//            System.out.println("All done!");
//            return;
//        }
//        System.out.println(num);
//        counting(num - 1);
//    }


}///end of class, don't delete

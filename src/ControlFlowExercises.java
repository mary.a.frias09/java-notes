import java.nio.file.LinkPermission;
import java.util.Scanner;

public class ControlFlowExercises {
    public static void main(String[] args) {

//Create an integer variable i with a value of 9
//        int i = 9;


// Create a while loop that runs so long as i is less than or equal to 23
// Each loop iteration, output the current value of i, then increment i by one
//   9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
//        while (i <= 23) {
//            System.out.print( i + " ");
//            i++;


//            System.out.print(i++ + " ");

//        }

// Do While
// Create a do-while loop that will count by 2's starting with 0 and ending at 100.
// Follow each number with a new line.

//        int i = 0;
//        do {
//            System.out.println(i);
//            i+=2;
//        } while (i <= 100);


// Alter your loop to count backwards by 5's from 100 to -10.

//        int i = 100;
//        do {
//            System.out.println(i);
//            i-=5;
//        } while (i >= -10);


// Create a do-while loop that starts at 2, and displays the number squared on each line while the number is less than 1,000,000.
// Output should equal:
//        2
//        4
//        16
//        256
//        65536

//       long num = 2;
//        do {
//            System.out.println(num);
//            num *= num;
//        } while (num <= 1000000);

//      using int as a reference
//        int num = 2;
//        do {
//            System.out.println(num);
//            num = (int)Math.pow(num, 2);
//        } while (num <= 1000000);


//        For Loop
//        Refactor the previous two exercises to use a for loop instead


//        Write a program that prints the numbers from 1 to 100.
//        For multiples of three print "Fizz" instead of the number
//        For the multiples of five print "Buzz".
//                For numbers which are multiples of both three and five print "FizzBuzz".
//                Display a table of powers.
//        Prompt the user to enter an integer.
//                Display a table of squares and cubes from 1 to the value entered.
//        Ask if the user wants to continue.
//        Assume that the user will enter valid data.
//        Only continue if the user agrees to.

        //FIZZBUZZ

//        for (int x = 0; x <= 100; x++) {
//            if (x % 15 == 0) {
//                System.out.println("Fizzbuzz");
//            }else if (x % 5 == 0) {
//                System.out.println("Buzz");
//            } else if (x % 3 == 0) {
//                System.out.println("Fizz");
//            } else {
//                System.out.println(x);
//            }
//        }


//        What number would you like to go up to? 5
//        Here is your table!
//                number | squared | cubed
//                ------ | ------- | -----
//                1      | 1       | 1
//        2      | 4       | 8
//        3      | 9       | 27
//        4      | 16      | 64
//        5      | 25      | 125

        //DISPLAY A TABLE OF POWERS
//        System.out.println("What number would you like to go to?");
//        Scanner input = new Scanner(System.in);
//        int num = input.nextInt();
//
//        System.out.println("Here is the table");
//        System.out.println("NUMBER | SQUARE | CUBED");
//        System.out.println("- - - -| - - - -| - - -");
//        for (int i = 1; i <= num; i++) {
//            int num1 = i;
//            int num2 = (int)Math.pow(i, 2);
//            int num3 = (int)Math.pow(i, 3);
//            System.out.println("\t" + num1 + "  | \t" + num2 + "   |\t " + num3);
////           // System.out.printf("%-6d | %-6d | %-6%n", num1, num2, num3);
//        }


//        Convert given number grades into letter grades.
//                Prompt the user for a numerical grade from 0 to 100.
//        Display the corresponding letter grade.
//                Prompt the user to continue.
//        Assume that the user will enter valid integers for the grades.
//        The application should only continue if the user agrees to.
//        Grade Ranges:
//        A : 100 - 90
//        B : 89 - 80
//        C : 79 - 70
//        D : 66 - 60
//        F : 59 - 0
//        Bonus
//        Edit your grade ranges to include pluses and minuses (ex: 98-100 = A+, 94-97 = A, 90-93 = A-).

        //
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Enter a grade number:");
//        int grade = scanner.nextInt();
//        if (grade >= 90 && grade <= 100) {
//            System.out.println("You got an A");
//        } else if (grade >= 80 && grade < 90) {
//            System.out.println("You got an B");
//        } else if (grade >= 70 && grade < 80) {
//            System.out.println("You got a C");
//        } else if (grade >= 60 && grade < 70) {
//            System.out.println("You got a D");
//        } else if (grade >= 0 && grade < 60) {
//            System.out.println("You got an F");
//        } else {
//            System.out.println("You did not enter a grade value (0-100)");
//        }
//        System.out.println("Would you like to continue?");
//        input = scanner.next();
//    } while
//
















    }//end of method, don't delete
}//end of class, don't delete

public class Staff {

// - A private nameOfStaff property that is a string.
//             private String nameOfStaff = "";


//- A private hourlyRate property that is a final integer.

//            private final int hourlyRate = 10;


//- The 'final' keyword indicates that the value cannot be changed after it is created.



//- A private hoursWorked property that is an integer.
//        private int hoursWorked = 58;


//    The first constructor accepts the student's ID and name and sets the value to put
//    properties based on what is passed to it.



//    It also sets a default value of the classroom property to the string Unassigned.



//    The second constructor allows us to specify values for all three the student's id,
//    name and the classroom when the object is created.

//    Student sOne = new Student(460710, "Stephen");
//    Student sTwo = new Student("Karen", "Omega");
//    System.out.println(sOne.getStudentInfo());
//    System.out.println(sTwo.getStudentInfo());









}//end of class

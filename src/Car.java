public class Car {

    //car example
    //instance variable objects
    public String name;
    public String maker;
    public String model;
    public int year;

    //instance method
    public String startTheCar() {
        return String.format("Starting up a %s %s %s %d", name, maker, model, year);
    }
    //the car "class" constructor
    public Car() {   //called when the object is created
        System.out.println("A car is being made!");
    }

    public static void main(String[] args) {
        //Create an instance of our class
        Car firstCar = new Car();  //instantiated object "new"
        firstCar.name = "Betty";
        firstCar.maker = "Chevy";
        firstCar.model = "Corvette";
        firstCar.year = 1975;
        System.out.println(firstCar.startTheCar());

        Car car2 = new Car();
        car2.name = "Speedy";
        car2.maker = "Cadillac";
        car2.model = "CTS";
        car2.year = 2020;
        System.out.println(car2.startTheCar());
    }














}







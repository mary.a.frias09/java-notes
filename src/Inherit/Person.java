package Inherit;

public class Person {//creating a class

    String name;

    public Person(String name) {
        this.name = name;
    }


    public void sayHello(){//sayHello method

        System.out.println("Hello from:" + name);//printing string

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

abstract class Employee {

    protected String name;

    protected String department;

    public Employee() {
    }

    public Employee(String name, String department) {

        this.name = name;

        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }



    public void setDepartment(String department) {
        this.department = department;
    }

    public abstract String work();

}

class Developer extends Employee{
    public String work(){
        return "Writing code";
    }
}

class Manager extends Employee {
    @Override
    public String work() {
        return "Working";
    }

//    public abstract class work(){

//    }
}


//
// class Employee extends Person {
//
//     private double salary;
//
//     public Employee(String name) {
//         super(name);
//     }
//
//     public void doWork() {
//         System.out.println("Get to work!!!");
//     }
//     //Method overriding happening below
//     public void sayHello(){
//         System.out.println("I say Hello differently! ");
//     }
//
// }

class SuperHero extends Person{

    private final String alterEgo;

    public SuperHero(String name, String alterEgo){
        super(name);

        this.alterEgo = alterEgo;
    }
    public String getName(){

        return alterEgo;
    }

    public String getSecretIdentity(){

        return super.getName();

    }
}

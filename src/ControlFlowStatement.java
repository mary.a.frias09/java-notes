import jdk.swing.interop.SwingInterOpUtils;

import java.util.Scanner;

public class ControlFlowStatement {


    public static void main(String[] args) {

        //COMPARISON OPERATORS
//       !=, <=, >, <=, >=,==
        //System.out.printIn( 5 != 2) ' //true
        //System.out.printIn( 5 >= 5) ' //true
        //System.out.printIn( 5 <= 5) ' //true



//      LOGICAL OPERATORS
//           ||, &&
//        System.out.println(5 == 6 && 2>1 && 3!=3); //false
//        System.out.println(5 != 6 && 2>1 && 3!=3); //true

//        System.out.println(5 == 5 || 3 != 3);  //true

//         IF STATEMENTS
        /*
        SYNTAX:
            if (conditional 1 is met) {
            do task 1
            } else if (conditional 2 is met) {
            do task 2
            }
            else {
            do task 3
            }
         */

//        int score = 34;
//        if (score >= 50) {
//            System.out.println("New High Score!");
//        } else {
//            System.out.println("Try again...");
//        }

//       STRING COMPARISON
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Would you like to continue? [y/N]");
//
//        String userInput = sc.next();


//        DON'T DO THIS
//        boolean confirm = userInput == "y";

//        INSTEAD DO THIS
//        boolean confirm = userInput.equals("y");
//        if (confirm == true) {
//            System.out.println("Welcome to the Jungle");
//        } else if (userInput.equals("n")) {
//            System.out.println("Welcome to the Jungle...You entered ya lowercase n");
//        } else {
//            System.out.println("Bye bye bye! #NYSYNC4LIFE");
//        }



//       DON'T DO THIS
//        if (userInput == "y") {
//            System.out.println("Welcome to the Jungle");
//        }


//        SWITCH STATEMENT
        /*
        switch (variable used for "switching( {
        case firstCase : do take A;
                         break:
        case secondCase: do task B;
                         break;
        default :        do task C;
                         break;
         */

//        Scanner input = new Scanner(System.in);
//        System.out.println("Enter your grade: ");
//        String userGrade = input.nextLine().toUpperCase();
//
//        switch (userGrade) {
//
//            case "A":
//                System.out.println("Distinction");
//                break;
//            case "B":
//                System.out.println("B Grade");
//                break;
//            case "C":
//                System.out.println("C Grade");
//                break;
//            case "D":
//                System.out.println("D Grade");
//                break;
//            default:
//                System.out.println("Fail...");
//        }

//      WHILE LOOP
     /*
        SYNTAX:
        while (conditional) {
        //loop
        }
     */
//        int i = 0;
//        while (i < 10) {
//            System.out.println("i is " + i );
//            i++;
//        }

//      DOWHILE LOOP
        /*
        do {
        //statements
        } while (condition is true)
         */
//        do {
//            System.out.println("You will see this!");
//        } while (false);

//        FOR LOOP

//        syntax:
//       for (var i = 0; i <= 10; i ++) {
//           System.out.println(i);
//       }

//      ESCAPE SEQUENCES - dealing with Strings
        System.out.println("Hello \nWorld");
        System.out.println("Hello \tWorld");














    } //END OF METHOD DON'T DELETE
} // END OF CLASS DON'T DELETE

public class Employee {




    public String work(){

        return "I am filling documents for work.";
    }
}

class Manager extends Employee {

    public String work(){


        return "I am the manager listen to me.";
    }
}

class PolymorphismDemo{

    public static void doWork(Manager e){

        System.out.println(e.work());

    }
    public static void doWork(Employee e){

        System.out.println(e.work());

    }
    public static void main(String[] args) {
        Manager one = new Manager();


        Employee two = new Employee();
        Employee three = new Employee();
        doWork(one);

        doWork(two);

        doWork(three);
    }
}


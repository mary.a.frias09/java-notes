package ArraysLecture;

import java.util.Scanner;

public class average {
    //created scanner to receive input from user
    public static Scanner scanner = new Scanner(System.in);

    //main method where we call our methods
    public static void main(String[] args) {
        //declaring an int array declaring a variable myInterArray and assigning it getIntegers method putting values
        int[] myIntArray = getIntegers(5);
        //print array called myIntArray
        printArray(myIntArray);
        //printing out the average by calling getAverage method takes in myIntArray
        System.out.println(" Average is " + getAverage (myIntArray));



    }//end of main method

    //creating a method to getIntegers takes ina number printout integers in the array
    public static int[] getIntegers(int number) {
        System.out.println("Please enter : " + number + " Integer values ");
        //declaring an array creating array object taking in int number within method
        int intArray[] = new int[number];
        //looping through numbers
        for(int i = 0; i < number; i++) {
            //assigning int array scanner to the nextInt
            intArray[i] = scanner.nextInt();
        }
        //returning array
        return intArray;

    }

    public static void printArray(int[] intArray) {
        //looping through array
        for(int i = 0; i < intArray.length; i++) {
            //printing out items indexed in array
            System.out.println(intArray[i]);
        }
    }

    //created method to getAverage
    public static double getAverage(int[] intArray) {
        //creating sum object and assigning value of 0
        int sum = 0;
        //looping through int array using i++ to perform sum
        for (int i = 0; i < intArray.length; i++) {
            //now assigning sum object to intArray
            sum += intArray[i];
        }
        return (double) sum / (double) intArray.length;
    }

}//end of class

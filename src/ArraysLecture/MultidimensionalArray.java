package ArraysLecture;

public class MultidimensionalArray {

    public static void main(String[] args) {
        //create a 2d array
        int [] [] a = {
                {1, 2, 3 },

                {4, 5, 6, 7},

                 {8},

                 {9}
        };

        //calculate length of each row
        System.out.println("First element of row 1 " + a[0][0]); // 1 [0] is row
        System.out.println("Third element in second row " + a[1][2]);  // 6
        System.out.println( a[2][0]); //8
        System.out.println( a[3][0]); //9


    }//end of method












}//end of class

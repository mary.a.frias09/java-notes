package ArraysLecture;

import java.util.Arrays;

public class ArraysLecture {
    //Arrays
    // List of data that contains zero or more items called elements
    //Arrays elements can only be of one datatype within a single array

    //SYNTAX for Array
    //declaring an array datatype double & assigning variable of prices
    double[] prices;
    int[] integers;
    String[] names;
    float[] floatingNumbers;

    public static void main(String[] args) {

        String[] names = new String[6]; //when using new you must assign the array value
        names[0] = "Adrian";
        names[1] = "Sandra";
        names[2] = "MaryAnn";
        names[3] = "Henry";
        names[4] = "Jonathan";
        names[5] = "Eric";
//        names[6] = "Instructors";

        System.out.println(names[0]);

        String[] avengers = {"Captain America", "Hulk", "Iron Man", "Black Widow", "Hawk eye", "Thor"};

//        System.out.println(avengers.length);

        //iterating over arrays
        int[] numbers = new int[5];

        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = 3;
        numbers[3] = 4;
        numbers[4] = 5;

//        for (int i = 0; i < numbers.length; i++) {
//            System.out.println(numbers[i]);
//        }

        String[] languages = {"html", "css", "javascript", "angular", "java"};
//        for (int i = 0; i < languages.length; i++)  {
//            System.out.println(languages[i]);

        //enhanced for loop with the same results of printing the
        for(String language : languages) {
            System.out.println(language);

        }

        //Example iterating through 2d array
        int[] [] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
            for(int i = 0; i < matrix.length; i++) {
                System.out.println(Arrays.toString(matrix[i]));
            };




    }//end of method



}//end of class

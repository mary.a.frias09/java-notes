package Shapes;

public class Rectangle extends Quadrilateral implements Measurable{

    protected double length;
    protected double width;

    public Rectangle(double length, double width) {
        super(length, width);
    }

//    Rectangle should define a constructor that accepts two numbers for length and width, and sets those properties.

//    public Rectangle(double length, double width) {
//        this.length = length;
//        this.width = width;
//    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }
     public void setWidth(double width) {
        this.width = width;
     }
//   Create two methods on the Rectangle class, getArea and getPerimeter that return the respective values.

     public double getArea() {  // area = length x width

         return length * width;
     }

     public double getPerimeter() {  // perimeter = 2 x length + 2 x width

         return (length * 2) + (width * 2);
     }




}//end of rectangle class





//Change your existing Rectangle class to inherit from Quadrilateral and implement Measurable.












//    Inside of the shapes directory, create a Measurable interface with the following methods:
//        double getPerimeter();
//        double getArea();
//        Create an abstract Shape class inside of the shapes directory.
//        Inside of shapes, create an abstract Quadrilateral class that extends Shape and implements Measurable.
//        This class should have:
//        protected properties for length and width
//        a constructor that accepts two numbers for the length and width and sets those properties methods for
//        getting the length and width
//        abstract methods for setting the length and width

//        Change your existing Rectangle class to inherit from Quadrilateral and implement Measurable.
//        Change your existing Square class to extend Quadrilateral.
//        Because the length of all sides of a square are the same, the methods for setting the length and the
//        width should set both protected properties.


//        Modify your ShapesTest class, use it to:
//        declare a variable of the type Measurable named myShape.
//        Test your code by creating instances of both Square and Rectangle and assigning them to myShape so you can
//        display the shape's area and circumference.


//        Answer the following questions:
//        Why does the code fail to compile if you leave off the getPerimeter method in Rectangle?
//        What happens if you try to call the getLength or getWidth methods of the myShape variable? Why?































//    It should have protected properties for both length and width.
//    protected double length;
//    protected double width;

//    Rectangle should define a constructor that accepts two numbers for length and width, and sets those properties.
//    public Rectangle(double length, double width) {
//        this.length = length;
//        this.width = width;
//    }

//    public double getLength() {
//        return length;
//    }

//    public void setLength(double length) {
//        this.length = length;
//    }

//    public double getWidth() {
//        return width;
//    }
//     public void setWidth(double width) {
//        this.width = width;
//     }
//   Create two methods on the Rectangle class, getArea and getPerimeter that return the respective values.

//     public double getArea() {  // area = length x width

//         return length * width;
//     }

//     public double getPerimeter() {  // perimeter = 2 x length + 2 x width

//         return (length * 2) + (width * 2);
//     }




//}//end of rectangle class


//        The formulas for both follow:
//        perimeter = 2 x length + 2 x width
//        area = length x width

package Shapes;



// class Cylinder extends Circle {
//    // private instance variable
//    private double height;
//
//    // Constructors
//    public Cylinder() {
//        super(radius);  // invoke superclass' constructor Circle()
//        this.height = 1.0;
//        System.out.println("Constructed a Cylinder with Cylinder()");  // for debugging
//    }
//    public Cylinder(double height) {
//        super(radius);  // invoke superclass' constructor Circle()
//        this.height = height;
//        System.out.println("Constructed a Cylinder with Cylinder(height)");  // for debugging
//    }
//    public Cylinder(double height, double radius) {
//      /**BUG*/ //  super(radius);  // invoke superclass' constructor Circle(radius)
//        super(radius);
//        this.height = height;
//        System.out.println("Constructed a Cylinder with Cylinder(height, radius)");  // for debugging
//    }
//    public Cylinder(double height, double radius, String color) {
//       /**BUG*/ // super(radius, color);  // invoke superclass' constructor Circle(radius, color)
//        super(radius);
//        this.height = height;
//        System.out.println("Constructed a Cylinder with Cylinder(height, radius, color)");  // for debugging
//    }
//
//    // Getter and Setter
//    public double getHeight() {
//        return this.height;
//    }
//    public void setHeight(double height) {
//        this.height = height;
//    }
//
//    /** Returns the volume of this Cylinder */
//    public double getVolume() {
//        return getArea()*height;   // Use Circle's getArea()
//    }
//
//    /** Returns a self-descriptive String */
//    public String toString() {
//        return "This is a Cylinder";  // to be refined later
//    }
//}
//public class Cylinder extends Circle {
//    public Cylinder(double radius) {
//        super(radius);
//    }
//
//    public Cylinder(int xCenter, int yCenter, double radius) {
//        super(xCenter, yCenter, radius);
//    }
//
//    public Cylinder(Point center, double radius) {
//        super(center, radius);
//    }
//    // Override the getArea() method inherited from superclass Circle
//    @Override
//    public double getArea() {
//        return 2*Math.PI*getRadius()*height + 2*super.getArea();
//    }
//    // Need to change the getVolume() as well
//    public double getVolume() {
//        return super.getArea()*height;   // use superclass' getArea()
//    }
//    // Override the inherited toString()
//    @Override
//    public String toString() {
//        return "Cylinder[" + super.toString() + ",height=" + height + "]";
//    }
//}

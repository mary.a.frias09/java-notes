package Shapes;

public class Square extends Quadrilateral {


    public Square(double side) {
        super(side, side);
    }

    @Override
    public void setLength(double length) {

        this.length = length;

    }

    @Override
    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double getPerimeter() {
        return (this.length * 4);
    }

    @Override
    public double getArea() {
        return Math.pow(this.length, 2);
    }
//
////        Square should define a constructor that accepts one argument, side,
//
//    public Square(double side) {
//// and calls the parent's constructor to set both the length and width to the value of side.
//
//        super(side,side);//rectangle constructor setting to the square constructor data type
//
//
//    }
//
//    //    In the Square class, override the getArea and getPerimeter methods with the following definitions for a square
////    perimeter = 4 x side
////    area = side ^ 2
//    public double getArea(){
//        return Math.pow(this.length, 2);
//    }
//
//    public double getPerimeter(){
//        return(this.length * 4);
//    }
//
//
}
//



//        Change your existing Square class to extend Quadrilateral.
//        Because the length of all sides of a square are the same, the methods for setting the length and the width should set both protected properties.







































//public class Square extends Rectangle {

    //    Square should define a constructor that accepts one argument, side,
//    public Square(double side)  {
//          calls the parent's constructor  to set both the length and width to the value of side.
//        super(side, side);  //rectangle constructor to square data type
//    }

//    override the getArea  area = side ^ 2
//    @Override
//    public double getArea() {
//        return Math.pow(this.length, 2);
//    }

//    override the getPerimeter    perimeter = 4 x side
//    @Override
//    public double getPerimeter() {
//        return (this.length * 4);
//    }


//}//end of Square class

//        Re-run your ShapesTest class.

//        How can you determine which getArea and getPerimeter methods are being called on each object?

package Shapes;

import java.awt.*;

/**
 * The Circle class composes a "Point" instance as its center and a radius.
 * We reuse the "Point" class via composition.
 */
public class Circle {
    // The private member variables
    private Point center;  // Declare an instance of the "Point" class
    private double radius;
    private String color;

    // Constructors
    /** Constructs a Circle instance with the default values
     * @param radius*/
    public Circle(double radius) {
        this.center = new Point(); // Construct a Point at (0,0)
        this.radius = 1.0;
    }
    /** Constructs a Circle instance with the center at (xCenter, yCenter) and radius */
    public Circle(int xCenter, int yCenter, double radius) {
        center = new Point(xCenter, yCenter); // Construct a Point at (xCenter,yCenter)
        this.radius = radius;
    }
    /** Constructs a Circle instance with the given Point instance as center and radius */
    public Circle(Point center, double radius) {
        this.center = center;  // The caller had constructed a Point instance
        this.radius = radius;
    }



    // Getters and Setters
    public double getRadius() {
        return this.radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    public Point getCenter() {
        return this.center;  // return a Point instance
    }
    public void setCenter(Point center) {
        this.center = center;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }



    /** Returns a self-descriptive String */
    public String toString() {
        return "Circle[center=" + center + ",radius=" + radius + "]";  // invoke center.toString()
    }

    /** Returns the area of this circle */
    public double getArea() {
        return Math.PI * radius * radius;
    }

    /** Returns the circumference of this circle */
    public double getCircumference() {
        return 2.0 * Math.PI * radius;
    }

    /** Returns the distance from the center of this circle to the center of
     the given Circle instance called another */
    public double distance(Circle another) {
        return center.distance(another.center);  // Use distance() of the Point class
    }
}
























































//package Shapes;
//
//public class Circle {
//
////   Two private instance variables: radius (of the type double) and color (of the type String),
////   with default value of 1.0 and "red", respectively.
//private double radius;
//
//private String color;
//
////   Two overloaded constructors - a default constructor with no argument,
////   and a constructor which takes a double argument for radius.
//
//public Circle() {
//    radius = 1.0;
//    color = "red";
//}
//public Circle(double radius, String color) {
//    this.radius = radius;
//    this.color = color;
//}
//
////   Two public methods: getRadius() and getArea(), which return the radius and area
////   of this instance, respectively.
//
//public double getRadius() {  //returns the radius getRadius()
//    return radius;
//}
////public double getArea() {  //returns the area getArea()
////    return area;
////}
//
//
////      Constructs a Circle instance with default value for radius and color */
/////  ** Constructs a Circle instance with the given radius and default color */
/////** Returns the radius */
/////** Returns the area of this Circle instance */
//
//    public double getArea () {
//    return radius*radius*Math.PI;
//}
//
//
//
/////**Compile "Circle.java". Can you run the Circle class? Why?*/
//
//
/////**This Circle class does not have a main() method. Hence, it cannot be run directly.
//// This Circle class is a "building block" and is meant to be used in another program.
//
//
//// Let us write a test program called TestCircle (in another source file called TestCircle.java) which uses the
//// Circle class, as follows*/
//
//
//
//
//
//
//
//
//
//
//
//
//}//end of class

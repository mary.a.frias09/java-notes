package Shapes;

public interface Measurable {
    double getPerimeter();
    double getArea();



}//end of measurable class



//    Inside of the shapes directory, create a Measurable interface with the following methods:
//        double getPerimeter();
//        double getArea();

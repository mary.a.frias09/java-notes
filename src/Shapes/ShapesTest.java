package Shapes;

//    Test your code by creating a new class, ShapesTest (also inside of shapes) with a main method.
public class ShapesTest {

    public static void main(String[] args) {
//        create a variable of the type Rectangle named box1 and assign it a new instance of the Rectangle class with a width of 4 and a length of 5

//            Rectangle box1 = new Rectangle(5,4);
// verify that the getPerimeter and getArea methods return 20 and 18, respectively.
//        System.out.println(box1.getArea());
//        System.out.println(box1.getPerimeter());



// create a variable of the type Rectangle named box2 and assign it a new instance of the Square class that has a side value of 5.
//        Rectangle box2 = new Rectangle();

//    verify that the getPerimeter and getArea methods return 20 and 25, respectively.
//
//        System.out.println(box2.getPerimeter());
//        System.out.println(box2.getArea());
//
//        Square box3 = new Square(7);
//        System.out.println(box3.getArea());
//        System.out.println(box3.getPerimeter());

        Measurable myShape = new Square(7);
        System.out.println(myShape.getArea());
        System.out.println(myShape.getPerimeter());

        Measurable myShape2 = new Rectangle(25,14);
        System.out.println(myShape2.getArea());
        System.out.println(myShape2.getPerimeter());



    }





}




//  Change your existing Square class to extend Quadrilateral.
//  Because the length of all sides of a square are the same,
//  the methods for setting the length and the width should set both protected properties.




//    Inside the main method, create a variable of the type Rectangle named box1 and assign it a new instance
//    of the Rectangle class with a width of 4 and a length of 5 verify that the getPerimeter and getArea methods return 18 and 20, respectively.
//        Rectangle box1 = new Rectangle(5, 4);
//        System.out.println(box1.getPerimeter()); //18
//        System.out.println(box1.getArea()); //20



//    create a variable of the type Rectangle named box2 and assign it a new instance of the Square class that has a side value of 5.
//        Rectangle box2 = new Square(5);

//    verify that the getPerimeter and getArea methods return 20 and 25, respectively.
//        System.out.println(box2.getPerimeter());  //20
//        System.out.println(box2.getArea()); //25



//        Square box3 = new Square(7);
//        System.out.println(box3.getPerimeter()); //28
//        System.out.println(box3.getArea());  //49



//    }//end of main method

//}//end of ShapesTest class

//        Re-run your ShapesTest class.
//        How can you determine which getArea and getPerimeter methods are being called on each object?


//STUDENT EXAMPLE

//JONATHAN'S SOLUTION
//package Shapes;
//public class ShapesTest {
//    public static void getPerimeter(Rectangle e){
//        System.out.println(e.getPerimeter());
//    }
//    public static void getArea(Rectangle e){
//        System.out.println(e.getArea());
//    }
//    public static void main(String[] args) {
//        Rectangle box1 = new Rectangle(5,4);
//        Rectangle box2 = new Square(5);
//        Square box3 = new Square(7);
//        getPerimeter(box1);
//        getArea(box1);
//        getPerimeter(box2);
//        getArea(box2);
//        System.out.println(box3.getPerimeter());
//        System.out.println(box3.getArea());
//    }
//}

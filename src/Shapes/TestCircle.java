package Shapes;

import java.awt.*;

/**
 * A test driver for the Circle class.
 */
public class TestCircle {
//    public static void main(String[] args) {
//        // Test Constructors and toString()
//        Circle c1 = new Circle(radius);
//        System.out.println(c1);  // Circle's toString()
//        Circle c2 = new Circle(1, 2, 3.3);
//        System.out.println(c2);  // Circle's toString()
//        Circle c3 = new Circle(new Point(4, 5), 6.6);   // an anonymous Point instance
//        System.out.println(c3);  // Circle's toString()
//
//        // Test Setters and Getters
//        c1.setCenter(new Point(11, 12));
//        c1.setRadius(13.3);
//        System.out.println(c1);  // Circle's toString()
//        System.out.println("center is: " + c1.getCenter());  // Point's toString()
//        System.out.println("radius is: " + c1.getRadius());
//
//
//
//        // Test getArea() and getCircumference()
//        System.out.printf("area is: %.2f%n", c1.getArea());
//        System.out.printf("circumference is: %.2f%n", c1.getCircumference());
//
//        // Test distance()
//        System.out.printf("distance is: %.2f%n", c1.distance(c2));
//        System.out.printf("distance is: %.2f%n", c2.distance(c1));
//    }
}








//package Shapes;

//import Shapes.Circle;

/////**Save as "TestCircle.java"

////import org.w3c.dom.ls.LSOutput;

//public class TestCircle {

//    public static void main(String[] args) {



//// // Declare an instance of Circle class called c1.
//// // Construct the instance c1 by invoking the "default" constructor
//// // which sets its radius and color to their default value.

//    Circle c1 = new Circle();


//// // Invoke public methods on instance c1, via dot operator.
//// // The circle has radius of 1.0 and area of 3.141592653589793

//     System.out.println("This circle has a radius of " + c1.getRadius() + " and an area of " + c1.getArea() + " and the color is " + c1.getColor());



//// // Declare an instance of class circle called c2.
//// // Construct the instance c2 by invoking the second constructor
//// // with the given radius and default color
////    System.out.println("The circle has a radius of 2.0 and area of 12.566370614359172")
//// // Invoke public methods on instance c2, via dot operator.
//// //The circle has radius of 2.0 and area of 12.566370614359172

//        Circle c2 = new Circle(2.0);
//
//    System.out.println("This circle has a radius of " + c2.getRadius() + " and an area of " + c2.getArea() +  " and the color is " + c2.getColor());








//// /** Now, run the TestCircle and study the results.*/



//}//end of method


//}//end of class

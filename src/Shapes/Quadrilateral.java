package Shapes;


public abstract class Quadrilateral extends Shape implements Measurable {

    protected double length;

    public double getWidth() {
        return width;
    }
    public double getLength() {
        return length;
    }
    protected double width;

    //CONSTRUCTOR
    public Quadrilateral(double length, double width){
        this.length = length;
        this.width = width;
    }

    public abstract void setLength(double length);

    public abstract void setWidth(double width);
}


//   Inside of shapes, create an abstract Quadrilateral class that extends Shape and implements Measurable.
//     This class should have:
//      protected properties for length and width
//      a constructor that accepts two numbers for the length and width and
//      sets those properties methods for getting the length and width
//      abstract methods for setting the length and width

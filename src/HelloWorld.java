import java.util.Scanner;

public class HelloWorld {
    //psvm
    public static void main(String[] args) {
        //sout
//        System.out.println("Hello, World!!");
//        System.out.println("code inside of the curly braces");
//      DATA TYPES
        //8 PRIMITIVE DATA TYPES
        byte age = 12;                      // Whole num from -128 to 127
        short myShort = -32768;             // Whole num fro -32768 to 32767
        int myInt = 120;                    // Integer (whole)
        long myLong = 123143254656L;        // Stores whole numbers from -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
        float myFloat = 123.456F;           // Floating point number
        double myDouble = 123.456;          // Stores fractional numbers. Sufficient for storing 15 decimal digits
        char myChar = 'a';                  // Store a single Character
        boolean myBoolean = true;           // Stores true or false values

//        int x = 10;
//        int y = 25;
//        System.out.println(x+y);
//        System.out.println("X plus Y = " + (x + y ));
//******************************************************************************************************************************************
                                            //   //   JAVA EXERCISE    //    //

//  Create an int variable named favoriteNum and assign your favorite number to it, then print it out to the console.

//            int favoriteNum = 7;
//        System.out.println(favoriteNum);


//  Create a String variable named myName and assign your first name as a string value to it, then print the variable out to the console.

//            String myName= "MaryAnn";
//        System.out.println(myName);


//  Change your code to assign a character value to myName. What do you notice?
            //char requires single quotes


//   Change your code to assign the value 3.14159 to myName. What happens?
//            String myName= "3.14159";
//        System.out.println(myName);

//   Declare an long variable named myNum, but do not assign anything to it.
//   Next try to print out myNum to the console. What happens?
//            long myNum ;
//            myNum =

//   Change your code to assign the value 3.14 to myNum. What do you notice?
//        long myNum= 3.14 ;
//        myNum =

//   Change your code to assign the value 123L (Note the 'L' at the end) to myNum.
//            float myNum = 123F;



//   Change your code to assign the value 123 to myNum.



//   Why does assigning the value 3.14 to a variable declared as a long not compile, but assigning an integer value does?



//   Change your code to declare myNum as a float. Assign the value 3.14 to it.
//            float myNum = 3.14f;

//   What happens? What are two ways we could fix this?
//   Copy and paste the following code blocks one at a time and execute them
       /*
       comment out all other var x in exercise*/
//
//        int x = 10;
//        System.out.println(x++);  //10
//        System.out.println(x);    //11
//        int x = 10;
//        System.out.println(++x);  //11
//        System.out.println(x);    //11


//   What is the difference between the above code blocks? Explain why the code outputs what it does.
//                Try to create a variable named class. What happens?


//   Object is the most generic type in Java. You can assign any value to a variable of type Object.
//   What do you think will happen when the following code is run?
//
//        String theNumberEight = "eight";
//        Object o = theNumberEight;
//        int eight = (int) o;
//        System.out.println(eight);


//   Copy and paste the code above and then run it. Does the result match with your expectation?

//   How is the above example different from this code block?
//        int eight = (int) “eight”;



//   What are the two different types of errors we are observing?
//       Rewrite the following expressions using the relevant shorthand assignment operators:
//        int x = 5;
//        x = x + 6;
//        x += 6;
//        System.out.println(x);  //11


//        int x = 7;
//        int y = 8;
//            y = y * x;
//        y *= x;
//        System.out.println(y);



//        int x = 20;
//        int y = 2;
//            x = x / y;
//            y = y - x;
//        y -= x;
//        System.out.println(y);  //-18



//   What happens if you assign a value to a numerical variable that is larger (or smaller) than the type can hold?
//   What happens if you increment a numeric variable past the type's capacity?
//      Hint: Integer.MAX_VALUE is a class constant (we'll learn more about these later) that holds the maximum value for the int type. (edited)

//        int z = Integer.MAX_VALUE + 1;
//        System.out.println(z);

        // PRINTING OUR OUTPUT
//        String name = "Bravo";
//        System.out.format("Hello there, %s. Nice to see you.", name);

//        String greet = "Hola";
//        System.out.format("%s, %s!", greet, name);

//        System.out.println(greet + ", " + name + "!");

        // SCANNER CLASS - GET INPUT FROM THE CONSOLE

        //scanner variable name
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter something:"); //prompt the user to enter data
        String userInput = scanner.next(); //obtaining the value the user input
        System.out.println(userInput); // souting the userInput value






    } //end of main method, don't delete!!



} // end of class don't delete!

package oop;

import oop.BankAccount;

public class BankAccountDemo {

    public static void main(String[] args) {
        //creating an account
        BankAccount a = new BankAccount();
        a.setBalance(99);


        //creating another account
        BankAccount b = new BankAccount();
        b.setBalance(88);

        System.out.println(a.getBalance());
        System.out.println(b.getBalance());

        BankAccount.setInterestRate(14.78);
//            System.out.println("First: $" + a.getInterestRate());
//            System.out.println("Second: $" + b.getInterestRate());

        System.out.println("Interest rate is " + BankAccount.getInterestRate() + "%");





        }




}//end of class dont

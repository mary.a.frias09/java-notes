package oop;

/**Object - an instance of a class
 * -has properties and methods
 * -Instantiated with the new keyword
 * Class - template or blue-print that we use for objects
 */



//creating class
public class BankAccount {
    //creating instance variable
    private double balance ;


    public  static double interestRate;

    public static double getInterestRate(){
        return interestRate;
    }
    public static double setInterestRate(double ir) {
        interestRate = ir;
        return ir;
    }


    //getter
    public double getBalance() {

        return balance;
    }
    //setter
    public void setBalance(double balance) {
        this.balance = balance;
    }







}//end of class

package Movies;
import Movies.Movie;
import Movies.MoviesArray;

import java.util.Scanner;

public class MovieApplication {
    public static void main(String[] args) {
        Movie.startMovieApplication();

    }


 //  *  You will need to give the user a list of options for viewing all the movies or by category.
//   *  Use your Input class to get input from the user and display the info based on their choice
//              (Look into input and remember that it has to be imported)
//   *  When a category is selected from that users, only that category of movies should be displayed
//   *  The application should continue to run until the user chooses to exit.
//         *  Example of how it should look like.

//         *  What would you like to see?
//         *  0 - exit
//         *  1 - View all Movies
//         *  2 - Comedy
//         *  3 - Drama
//         *  4 - Animated
//         *  5 - Horror
//         *  6 - Sci-fi
//         *  Enter your selection:
//            *
//         *  List of Movies will be displayed here


}//end of class


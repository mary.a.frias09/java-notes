import java.util.Scanner;

public class ConsoleExercise {

    public static void main(String[] args) {

//        double pi = 3.14159;  //double variable name, pi is

//        Don't change the value of the variable, instead, reference one of the links above and use System.out.format to accomplish this.
//        System.out.println("The value of pi is approximately" + pi); // prints 3.14159
//        System.out.format("The value of pi is approximately %.2f", pi); // prints 3.14

//   Explore the Scanner Class
        Scanner scanner = new Scanner(System.in);

//   Prompt a user to enter a integer and store that value in an int variable using the nextInt method.
//        System.out.print("Enter an integer: ");
//        int userInput = scanner.nextInt();
//        System.out.print(userInput);
//   What happens if you input something that is not an integer?
        //mismatchexception

//   Prompt a user to enter 3 words and store each of them in a separate variable, then display them back, each on a newline.
//        System.out.println("Enter three words: ");
//        String word1 = scanner.next();
//        String word2 = scanner.next();
//        String word3 = scanner.next();

//        System.out.println(word1 + "\n" + word2 + "\n" + word3);

//        System.out.println(word1);
//        System.out.println(word2);
//        System.out.println(word3);

//   What happens if you enter less than 3 words?

//   What happens if you enter more than 3 words?

//   Prompt a user to enter a sentence, then store that sentence in a String variable using the .next method, then display that sentence back to the user.
//   Do you capture all of the words?
//   Rewrite the above example using the .nextLine method.

//        System.out.println("Enter a sentence: ");
//        String userInput = scanner.nextLine();
//        System.out.println(userInput);


//   Calculate the perimeter and area of Code Bound's classrooms
//   Use the .nextLine method to get user input and cast the resulting string to a numeric type.
//   Assume that the rooms are perfect rectangles.
//   Assume that the user will enter valid numeric data for length and width.

        // Solution found online to question
//        System.out.println("What is the length and width of the classroom? ");
//        System.out.print("L:");
//        Scanner s = new Scanner(System.in);
//        try{
//            // convert the string read from the scanner into Integer type
//            Integer length = Integer.parseInt(s.nextLine());
//            System.out.print("W:");
//            s = new Scanner(System.in);
//            Integer width = Integer.parseInt(s.nextLine());
//            // Printing the area of rectangle
//            System.out.println("Area of rectangle:"+ width * length);
////        }
////        catch(NumberFormatException ne){
////            System.out.println("Invalid Input");
//        }
//        finally{
//            s.close();
//        }

//        System.out.println("What is the length and width of the classroom? ");
//        String length = scanner.nextLine();
//        int l = Integer.parseInt(length);
//
//        String width = scanner.nextLine();
//        int w = Integer.parseInt(width );
//
//        System.out.println("The perimeter of the classroom is " + (2* w + 2 * l));
//        System.out.println("The area of the classroom is " + (w * l));


//   The area of a rectangle is equal to the length times the width, and the perimeter of a rectangle is equal to 2 times the length plus 2 times the width.
//   Bonuses


// 	Accept decimal entries
        System.out.println("What is the length and width of the classroom? ");
        String length = scanner.nextLine();
        Double l = Double.parseDouble(length);

        String width = scanner.nextLine();
        Double w = Double.parseDouble(width );

        System.out.println("The perimeter of the classroom is " + (2* w + 2 * l));
        System.out.println("The area of the classroom is " + (w * l));




//	Calculate the volume of the rooms in addition to the area and perimeter







    }//end of main method, don't delete!

} //end of class, don't delete!

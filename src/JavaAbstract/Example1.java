package JavaAbstract;

//using abstract class inheriting one abstract class at a time
public class Example1 {

    public void display1 () {
        System.out.println("display one method.");
    }


}//end of Example1 class


    abstract class Example2 {
        public void display2() {
            System.out.println("display 2 method ");
        }
    }

    abstract class Example3 extends Example1 {
        abstract void display3();
    }

    class Example4 extends Example3 {
        void display3() {
            System.out.println("I am on display...");
        }
    }

    class Demo{
        public static void main(String[] args) {
            Example4 obj = new Example4();
                    obj.display3();
        }
    }

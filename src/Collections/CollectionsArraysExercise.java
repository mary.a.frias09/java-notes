package Collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class CollectionsArraysExercise {
    public static void main(String[] args) {



//    Create a program that will print out an array of integers.

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);
        System.out.println(numbers);



//    Create a program that will print out an array of strings
        //creating ArrayList using asList method
        ArrayList<String> colors = new ArrayList<>(Arrays.asList(
                "Blue",
                "Yellow",
                "Pink",
                "Green"
        ));
        System.out.println(colors);

        ArrayList<String> colors1 = new ArrayList<>();
        colors1.add("Blue");
        System.out.println(colors1);



//    Create a program that iterates through all elements in an array.
//    The array should hold the names of everyone in Bravo Class
        ArrayList<String> Bravo = new ArrayList<>();
        Bravo.add("MaryAnn");
        Bravo.add("Sandra");
        Bravo.add("Jonathan");
        Bravo.add("Adrian");
        Bravo.add("Eric");
        Bravo.add("Henry");
        for(String s: Bravo){
            System.out.println(s);
        }

//    Create a program to insert elements into the array list.
//    This element should be added on the first element and last.
//    You can use a previous array or create a new one.
        ArrayList<String> animals = new ArrayList<>(Arrays.asList(
                "Monkey",
                "Giraffe",
                "Lemur",
                "Peacock",
                "Zebra"
        ));
        animals.add(0, "Ferret");
        System.out.println(animals);
        animals.add(5,"Parrot");
        System.out.println(animals);



//    Create a program to remove the third element from an array list.
//    You can create a new array or use a previous one.
        animals.remove(2);
        System.out.println(animals);


//    Create an array of Dog breeds.
//    Create a program that will sort the array list.
        ArrayList<String> dogs = new ArrayList<>();
        dogs.add("Maltipoo");
        dogs.add("Boxer");
        dogs.add("Labrador");
        dogs.add("Bull Dog");
        dogs.add("Mastiff");
        dogs.add("Chihuahua");
        System.out.println(dogs);

        Collections.sort(dogs);
        System.out.println(dogs);


//    Create an array of cat breeds.
//    Create a program that will search an element in the array List.
        ArrayList<String> cats = new ArrayList<>();
        cats.add("Persian");
        cats.add("Bengal");
        cats.add("Siamese");
        cats.add("Sphynx");
        cats.add("Ragdoll");
        cats.add("Main Coon");
        if(cats.contains("Cheetah")) {
            System.out.println("I am a cat who is a cheetah");
        }else
        System.out.println(cats);


//    Now create a program that will reverse the elements in the array.
        Collections.reverse(cats);
        System.out.println(cats);

    }//end of method
}//end of collections class

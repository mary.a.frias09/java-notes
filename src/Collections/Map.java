package Collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;


/**
 * The Map interface of the Java collections framework provides the functionality of the map data structure
 * In Java, elements if Map are stored in key/value pairs.
 * Keys are unique values associated with individual values.
 * A map can not contain duplicate keys, and each key is associated with single value
 *
 * Note: the Map interface maintains 3 different sets
 * the set of keys
 * the set of values
 * the set of key/value associations individually
 * Since Map is an interface we can not create object from it.
 *
 * In order to use functionalities of the Map interface, we can use these classes below:
 * HashMap
 * EnuMap
 * LinkedHashMap
 * WeakHashMap
 * TreeMap
 *
 * The Map interface is also extended by theses sunInterfaces:
 * sortedMap
 * NavigableMap
 * ConcurrentMap
 *
 *Common methods
 * .putIfAbsent - puts only a key-value pair if absent
 * .remove - removes key/value
 * .replace - replaces value at given key
 * .clear -
 * .isEmpty -
 *
 *
 * */





public class Map {

    public static void main(String[] args) {
        //creating a hashMap within map each one contains a capacity && a load factor
        HashMap<String, Integer> map1 = new HashMap<>();

        //Example 2 Creating HashMap with 30 as initial capacity
        HashMap<String, Integer> map2 = new HashMap<String, Integer>(30,1);

        HashMap<Integer, String> map = new HashMap<>();
        map.put(21 , "Twenty One ");
        map.put(31 , "Thirty One ");

//        Iterator<Integer>KeySetIterator = map.keySet().iterator();
//        while(KeySetIterator.hasNext()){
//            Integer key = KeySetIterator.next();
//
//            System.out.println("Key: " + key + " Value: " + map.get(key));

//            System.out.println("Does the HashMap contain 21 as a value: " + map.containsValue(21));

            //Defining HashMap
            HashMap<String, String> usernames = new HashMap<>();

            //adding using the put method
            usernames.put("Jim" , "Doe");
            usernames.put("Jane" , "Doe");
            usernames.put("Johnny" , "Cash");
            usernames.put("John" , "Lennon");

            System.out.println(usernames);

            //obtaining values in HashMap by using key
            usernames.get("John");
//            System.out.println(usernames);
//            System.out.println(usernames.get("John"));
            //check if key values are present
            System.out.println(usernames.containsKey("Johnny"));

        }


















}//end of Map class

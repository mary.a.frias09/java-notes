package Collections;

import java.util.ArrayList;

public class Clone {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<>();

        //adding arrayList
        al.add("apple");
        al.add("orange");
        al.add("mango");
        al.add("grape");
        System.out.println("ArrayList is " + al);

        ArrayList<String> al2 = (ArrayList<String>)al.clone();
        System.out.println("Shallow copy of ArrayList " +al2);


        //add and remove on original arraylist
        al.add("Fig");  //adding fig
        al.remove("grape");  //removing grape
        System.out.println(al); //printing out al arraylist

        //Display array list after add and remove
        System.out.println("Cloned arrayList: " + al2);
        System.out.println("original arraylist: " + al);



    }
}

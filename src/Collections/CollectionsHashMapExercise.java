package Collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

public class CollectionsHashMapExercise {
/*
       TODO:
        Create a program that will append a specified element to the end of a hash map.
        Create a program that will iterate through all the elements in a hash map
        Create a program to test if a hash map is empty or not.
        Create a program to get the value of a specified key in a map
        Create a program to test if a map contains a mapping for the specified key.
        Create a program to test if a map contains a mapping for the specified value.
        Create a program to get a set view of the keys in a map
        Create a program to get a collection view of the VALUES contained in a map.
*/
    public static void main(String[] args) {
        // Create an empty hash map
        HashMap<String, String> country = new LinkedHashMap<>();

        // Add elements to the map
        country.put("England", "London");
        country.put("Ireland", "Dublin");
        country.put("Italy", "Rome");
        country.put("Germany", "Munich");
        country.put("United States", "San Antonio");
        country.put("Australia", "Wales");

//        Create a program that will append a specified element to the end of a hash map.
        System.out.println(country);
        country.put("Morocco", "Marrakesh");

//        Create a program that will iterate through all the elements in a hash map
        for(String i : country.keySet()){
            System.out.println("Key: " + i + " \nValue: " + country.get(i));
        }

        // Print size and content
//        System.out.println("\n1. The Hash Map: " + country);

        // Checks whether the HashMap is empty or not
        boolean results = country.isEmpty();
        System.out.println("Is my HashMap empty? " + results);
//        ***********
//        if (country.isEmpty()) ;
//        System.out.println("\n2. HashMap Country is not empty");

//        Create a program to get the value of a specified key in a map
        System.out.println(country.get("United States"));
//        System.out.println("\n3. Map1 : " + country.put ("Italy", "Rome "));

        // Accessing the contents of HashMap through Keys
        System.out.println("\n4. Does key 'Ireland' exists?");
        if (country.containsKey("Ireland")) {
            System.out.println("Yes! - " + country.get("Ireland"));
        } else {
            System.out.println("No!");
        }

//        Create a program to test if a map contains a mapping for the specified key.
        HashMap<String,Integer> people = new HashMap<>();
        people.put("Jim", 21);
        people.put("Jane", 33);
        people.put("Jill", 11);
        people.put("Janna", 32);
        people.put("John", 24);
        people.put("Jeremiah", 35);
        people.put("Jack", 15);
//        System.out.println(people.remove("John"));  //removing John from the hashmap
        if(people.containsKey("John")){
            System.out.println("John is here as a key!");
        }else{
            System.out.println("No John here my friend!");
        }

//        Create a program to test if a map contains a mapping for the specified value.
        if(people.containsValue(10)){
            System.out.println("You have a value of 10!");
        } else {
            System.out.println("No value of such here...");
        }

//        Create a program to get a set view of the keys in a map
        Set<String> keySet = people.keySet();
        System.out.println("Key set values are " + keySet);

//        Create a program to get a collection view of the VALUES contained in a map.
        System.out.println("Collection view is: " + people.values());



    }//end of main



}//end of collections class

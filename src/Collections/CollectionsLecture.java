package Collections;

import java.util.ArrayList;

class Student{
    int roll;
    String name;
}

public class CollectionsLecture {
/** Collections are there are 2 main types of collections used in Java these are:
    * ArrayList
    * Hashmap
    * -Is a data structure that can be used to group or collect objects .
 * */


//ArrayList
    //represents an array that can be changed in size.
    //All elements in an array must be of the same object and datatype.
    /*
    .size() returns the number of elements in the array
    .add() adds an element to the collection at the specified index
    .get() returns the element specified at the index
    .indexOf returns the first found index of the given item. If given item not found returns -1
    .contain() checks to see if the arrayList contains the given element
    .lastIndexOf() finds the last index of the given element
    .isEmpty checks to see if the list is empty
    .remove() removes the first occurrence  of an item or an item at given index
     */

    //EXAMPLES

    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<String>(); //declared a new arraylist and defined (new) given datatype

        list1.add("Jane");
        list1.add("Jill");
        list1.add("Jack");
        list1.add("Jim");

        list1.remove("Jane");


        System.out.println("list of list1 is:" + list1);

        Student s1 = new Student ();

        s1.roll = 3244;
        s1.name = "June";

        ArrayList list2 = new ArrayList();

        list2.add("Jim");
        list2.add(10);
        list2.add(s1);

        System.out.println("list of list 2 is: " + list2);


        ArrayList<Integer> numbers1 = new ArrayList<Integer>();

        numbers1.add(10);
        numbers1.addAll(list2);
        System.out.println(numbers1);




    }










}//end of collectionsLecture class


package Collections;

import java.lang.reflect.Array;
import java.util.*;

/** List interface- an ordered or sequential collection of objects.
 * Has some methods which can be used to store and manipulate the ordered collection
 * The classes which implement the list interface are called as lists .
 * These classes are:
 * ArrayList
 * Vector
 * LinkedList
 * We can also use the list interface since ArrayList is implemented from List
 * In list you have control over the location on where an element is inserted or removed
 * All 15 methods of collection util library are available in addition to
 * :
 * E get(int index) - returns element at specified index
 * E set(int index, E element) - Replaces en element at the specified position with the passed element.
 * void add (int index, E element) - Inserts passed element at a specified index
 * Int index (object o ) - It returns an index of first occurrence of passed object
 * int lastIndexOf(Object o) - returns an index of last occurrence of passed object
 * ListIterator<E>listIterator() = It returns a list iterator over the elements of this list.
 * ListIterator<E>listIterator(int index) - it returns a list iterator over the elements of this list starting at the specified index
 * List<E>sublist(int fromIndex, int toIndex) - returns sub list of this list starting from index to index

    Examples
 * ArrayList
 * Vector
 * LinkedList
 */
public class List {

    public static void main(String[] args) {




    //syntax for creating instance of a ArrayList

    ArrayList a = new ArrayList(); //Example 1 (most commonly used)

    //linkedList example
    LinkedList  b = new LinkedList(); //

    //Vector
    Vector c = new Vector();

    //Stack list
    Stack d = new Stack();

    //Here is how we can create and ArrayList in Java
    //Here, Type indicates the type of an array list

    //1) ArrayList<Type> arrayList = new ArrayList<>();
 //   ArrayList <Integer> numbers = new ArrayList<>();

    //Initialize an ArrayList Using asList();
 //   new ArrayList<>(Arrays.asList(("Cat", "Dog", "Cow"));

    //We can use ArrayList using the list interface
 //    List<String> list = new ArrayList();

    //Example of creating a string type of ArrayList
        ArrayList<String> words = new ArrayList<>();
        //calling on collections methods from java.util
        words.add("Dog");
        words.add("Cat");
        words.add("Cow");

        System.out.println("ArrayList: " + words);

        //We can also add using indexes
        words.add(3, "Mouse");
        words.add(4, "Sheep");
        words.add(5, "Zebra");
        System.out.println("ArrayList adding with indexing : " + words);


        //Create an Integer arrayList, integers it the corresponding wrapper class of int type
        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(3);
        nums.add(4);
        System.out.println("ArrayList as an integer data type is: " + nums);

        //Example asList method
     //   ArrayList<String> groceries = new ArrayList<>();(Arrays
        //Then use the asList method to convert array into arrayList
     //   asList("Milk", "Chips", "Bread");
     //   System.out.println("ArrayList " + groceries);


    //Access elements of the arrayLIst
     //   String element


    }//end of main



    //LinedList are linked together using pointers<>, each element of the linked list references to the next element of the linkedList
    //Each element of the linkedList called the Node.
    //Each Node of the linkedList contains two items.
    //1) Content of the element
    //2)Pointer/Address/Reference to the next Node in the list.
    //Insert and delete operations in the linked list are not performance wise expensive because adding and deleting an element from the linked list doesn't require the element to shift.
    //In the following example we are using add(), addFirst(), and addLast() methods to add elements to the desired locations in the linkedList

//    public void linkedList() {
//        LinkedList<String> list = new LinkedList<String >();
//
//
//        //Adding elements to the linkedList
//        list.add("Steve");
//        list.add("Stan");
//        list.add("Stephanie");
//        list.add("Stacy");
//        System.out.println(list);
//    }


        //using iterator interface
//        Iterator<String> iterator = list.iterator();
//        while()



//
//            public static class stackExample {
//            public ArrayList stack = new ArrayList();
//
//            public stackExample(ArrayList stack) {
//                this.stack = stack;
//            }
//
//            public void push(Object obj){
//                //adding object to stack
//                stack.add(obj);
//            }
//
//            public Object pop(){
//                //Returns and remove the top item from the stack throws an EmptyStackException if there are no elements in the stack
//                if(stack.isEmpty()) //conditional if stack is empty
//                    throw new EmptyStackException(); //exception error
//                return stack.remove(stack.size()-1); //calling stack size and remove from index at -1
//            }
//
//            public boolean isEmpty(){
//                //test whether
//            }

//            }








}//end of class

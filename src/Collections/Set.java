package Collections;
//Set is a child interface of collection
//If we want to represent a group of individual objects as a single entity where duplicates are not allowed and insertion order is not preserved we would want to go for set.

import java.util.*;

/** A java set does not make any promises about the order of the elements kept internally
 * Properties of set
 * Contains only unique elements (not common and no duplicated no two elements are the same)
 * Order of elements in a set is implementation dependent
 * Hashset elements are order on hash code of elements
 * This interface models the mathematical set abstraction and is used to represent sets, (ex. deco of card is a set)
 *
 * the Java collection platform, contains three general Set implementations: HashSet, TreeSet, and LinkedHashSet, you can use iterator of foreach loop to traverse the elements of a set.
 * */
public class Set {
    private static Object Collection;

    /**
     * HashSet class internally uses HashMap to store the objects.
     * The elements you enter into HashSet will be stored as Keys of HashMap and their value will be constant.
     * HashSet does not allow duplicates, if yo insert a duplicate, the older element wil be overwritten.
     * */
    public static void main(String[] args) {



    //Creating a HashSet object;
    HashSet<String> set = new HashSet<String >();
    //Here we have created a HashSet called set

    HashSet<Integer> numbers = new HashSet<>();

        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
//        System.out.println("Numbers HashSet " + numbers);


        //Calling iterator method
        Iterator<Integer> iterate = numbers.iterator();
        System.out.println("HashSet using iterator: ");
        //accessing elements below
        while(iterate.hasNext()){
            System.out.println(iterate.next() + ".");
//            System.out.println(".");

            //Using remove()  method
            boolean value1 = numbers.remove(2);
            System.out.println("Is 2 removed? " + value1);
            System.out.println(numbers);


            //Using treeSet
            TreeSet<Integer> trees = new TreeSet<>();
            //adding elements to the TreeSet
            trees.add(20);
//            trees.add(30);
            trees.add(40);
            trees.add(50);
      //      System.out.println("Your TreeSet is: " + trees);

            //Using the higher method - returns the lowest element among the elements greater than the specified element.
            System.out.println("Using higher method: " + trees.higher(40));

            //Using the lower method - returns the greatest element among those elements that are less than the specified index.
            System.out.println("Using lower method: " + trees.lower(60));

            //Using the ceiling method - returns the lowest element among those that are greater than the specified element. If the element passed exists in a treeSet, it returns the element as an argument
            System.out.println("Using ceiling method: " + trees.ceiling(40));

            //Using the floor method - returns the greatest element among those elements that are less than the specified element. If the element passed exists in a treeSet, it returns the element passed as an argument
            System.out.println("Using floor method: " + trees.floor(30));

/**
 * LinkedHashSet = is an ordered version of the HashSet that maintains a doubly-linked list across all elements. Use this class instead of HashSet when you care about iteration order.
 * When you iterate through a HashSet the order is unpredictable, while a linkedHashSet lets you iterate through the elements in which they were inserted.
 * linkedHashSet Constructors
 * */
           new LinkedHashSet(); //Default constructor
  //         new LinkedHashSet(Collection);  //Creates a LinkedHashSet from Collection c

   //         new LinkedHashSet(int capacity); //Initial capacity as a parameter
            LinkedHashSet<String> linkedSet = new LinkedHashSet<String>();
            //adding element to linksSet
            linkedSet.add("BMW");
            linkedSet.add("Audi");
            linkedSet.add("Volkswagen");
            linkedSet.add("Honda");
            linkedSet.add("BMW");
            System.out.println(linkedSet);
            System.out.println("Size of linkedSet: " + linkedSet.size());

            System.out.println("Removing BMW from linkSet " + linkedSet.remove("BMW"));
            System.out.println(linkedSet);


        }
    }












}//end of set class

import java.util.Arrays;
import java.util.stream.StreamSupport;

public class StringMethodsPractice {
    public static void main(String[] args) {


        String myLength = "Good afternoon, good evening, and good night";
//      Write some java code that will display the length of the string

        System.out.println(myLength.length());


//      Write some java code that will display the entire string in all uppercase
        System.out.println(myLength.toUpperCase());

//      Use the same code but instead of using the toUpperCase() method, use the toLowerCase() method.
        System.out.println(myLength.toLowerCase());

//      Print it out in the console, what do you see?
//        Copy this code into your main method

        String firstSubstring = "Hello World".substring(6);
        System.out.println(firstSubstring);
//        Print it out in the console, what do you see?
         //World

//        Change the argument to 3, print out in the console. What do you get?
         //lo World

//        Change the argument to 10, print it out in the console. What do you get?
          //d

//        Copy this code into your main method
//        String message = "Good evening, how are you?";
//        System.out.println(message);

//        Using the substring() method, make your variable print out "Good evening"
//        System.out.println(message.substring(0,12));

//        Now using the substring() method, try to make your variable print out "how are you?"
//        System.out.println(message.substring(14,26));

//        Create a char variable named 'myChar' and assign the value "San Antonio"
//        String myChar = "San Antonio";
//        System.out.println(myChar);

//        Using the charAt() method return the capital 'S' only.
//        System.out.println(myChar.charAt(0));

//       Change the argument in the charAt() method to where it returns the capital 'A' only.
//        System.out.println(myChar.charAt(4));

//       Change the argument in the charAt() method to where it returns the FIRST lowercase 'o' only.
//        System.out.println(myChar.charAt(7));

//       Create a String variable name 'bravo' and assign all the names in the cohort in ONE string.
        String bravo = "Maryann Sandra Henry Eric Adrian Jonathan";
        System.out.println(bravo);

//       Using the split() method, create another variable named 'splitBravo' where it displays the list of names separated by a comma.
        String[] splitBravo = bravo.split(" ");
        System.out.println(Arrays.toString(splitBravo));

        System.out.println(Arrays.toString(bravo.split(",")));

//       Print out your results in the console
//        Ex. [Peter, John, Andy, David];

//        Use the following string variables.
                String m1 = "Hello, ";
                String m2 = "how are you?";
                String m3 = "I love Java!";

//        Using concatenation, print out all three string variables that makes a complete sentence.
        System.out.println( m1 +  m2 + " " + m3);

//
//        Use the following integer variable

        int result = 89;

//        Using concatenation, print out "You scored 89 marks for your test!"
//         *89 should not be typed in the sout, it should be the integer variable name.
        System.out.println("You scored " + result + " marks for your test!");
//***********************************************************************************************************************

//   For each of the following output examples, create a string variable named message that contains the desired output and print it out to the console.
//
//       * Do this with only one string variable and one print statement.
//          I never wanna hear you say
//          I want it that way!


//          Check "this" out!, "s inside of "s!
//          In windows, the main drive is usually C:\
//   I can do backslashes \, double backslashes \\,
//   and the amazing triple backslash \\\!

        //STRING DRILLS
//        String message = " I never wanna hear you say\nI want it that way";
        String message  = "Check \"this\" out!, \"s inside of \"s!\"";
        System.out.println(message);



//        String message = "In windows, the main drive is usually C:\\\n" +
//                "I can do backslashes \\, double backslashes \\\\,\n" +
//                "and the amazing triple backslash \\\\\\!";
//        System.out.println(message);



//        BONUS
//        Create date format converter application.
//                Take in the following format:
//        MM/DD/YYYY
//        Output the following:
//        MonthName DD, YYYY
//        Example:
//        user input - 12/01/1999
//        console output - December 1, 1999











































    }//end of method, don't delete
}//end of class, don't delete

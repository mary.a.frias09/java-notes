public class Human {

// Create Person class inside of src that has a private name property that is a string, and the following methods:
//        private String name = "John";

    public String firstName;
    public String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Human() {

    }

    public String getName() {
        return String.format("%s %s", firstName, lastName);
    }

    public static void main(String[] args) {
        Human firstHuman= new Human();
        firstHuman.firstName = "John";
        firstHuman.lastName = "Doe";

        System.out.println(firstHuman.getName());
    }



















//    public Person(){}
//    public Person(String name){
//        this.name = name;
//    }
//    returns the person's name
//    public String getName();
//    // changes the name property to the passed value
//    public void setName(String name);
//    // prints a message to the console using the person's name
//    public void sayHello();
//    The class should have a constructor that accepts a string value and sets the person's name to the passed string.
//    Create a main method on the class that creates a new Person object and tests the above methods.

//    public String getName(){
//TODO: return the person's name
//    }
//    public void setName(String name){
//TODO: change the name property to the passed value
//    }
//    public void sayHello(){
//TODO: print a message to the console using the person's name
//    }
//    The class should have a constructor that accepts a `String` value and sets
//    the person's name to the passed string.
//    Create a `main` method on the class that creates a new `Person` object and
//    tests the above methods.























}// end of class

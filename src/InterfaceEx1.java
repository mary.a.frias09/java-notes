interface InterfaceEx1 {//interface1
    public void display1(); //abstract method to be implemented on object creation


}//end of interface class
 interface Example2{  //example2 has an abstract method of display 2
    public void display2();
 }


 //this interface is extending both the above interface
interface Example3 extends InterfaceEx1, Example2{//this interface is using inheritance to extend Interface inter and interface Example2
}

class Example4 implements Example3 {
    @Override
    public void display1() {
        System.out.println("Display 2 method");

    }


    @Override
    public void display2() {
        System.out.println("Display 3 method");

    }
}

 class Demo{
     public static void main(String[] args) {
         Example4 obj = new Example4();
         obj.display1();
     }
 }

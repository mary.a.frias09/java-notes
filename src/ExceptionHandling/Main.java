package ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

//        int age;
//        System.out.println("Welcome to the club !");
//        System.out.println("Please enter your age:");
//
//        try {
//            // what we want to do here...
//            age = userInput.nextInt();
//
//            if(age < 0 || age > 125 ) {
//                System.out.println("Invalid age");
//                System.out.println("Age must be between 0 and 124");
//            } else if(age < 18){
//                System.out.println("Sorry, you're underage");
//            } else if(age > 21){
//                System.out.println("Welcome in!");
//            }
//        }
//        catch (InputMismatchException e) {
//            System.out.println("Hey, I said to enter your age please!");
//        }
//        finally {
//            System.out.println("This block will always run, not matter what");
//        }

        //Example 2
        int numerator, denominator;

        try {
            //what we want to do ...
            System.out.println("Enter the numerator: ");
            numerator = userInput.nextInt();

            System.out.println("Enter the denominator: ");
            denominator = userInput.nextInt();

            System.out.println("The result is: " + numerator/denominator);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println(" - - - End of error handling example - - - ");
        }





















    }//end of main method


}//end of main class

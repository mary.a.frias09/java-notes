package FileHandling;

import java.awt.*;
import java.io.*;

/*
File, BufferedReader, FileReader, BufferedWriter, and FileWriter from java.io package
How to import>
import java.io*;
 */
public class FileHandling {

    public static void main(String[] args) {
//      READING IF THERE'S A TEXT FILE WITH A NAME...use the FileReader class
        /*
        FileReader reads the content of a file
        - needs to be WRAPPED by a BufferedReader object
         */
        // create a string variable
//        String line;

        //create a BufferedReader object
//        BufferedReader bufferedReader = null;

        //try-catch-finally statement
//        try {
//            bufferedReader = new BufferedReader(new FileReader("./movieQuotes/hamilton.txt"));
//            line = bufferedReader.readLine();
//            while (line != null) {
//                System.out.println(line);
//                line = bufferedReader.readLine();
//            }
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        } finally {
//            try {
//                if (bufferedReader != null) {
//                    bufferedReader.close();
//                }
//            } catch (IOException e) {
//                System.out.println(e.getMessage());
//            }
//        }

//       WRITING TO A TEXT FILE...

        // CREATE string variable
//        String text = "I'll be back.";
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./movieQuotes/terminator.txt", true))) {
//            {
//                writer.write(text);
//                writer.newLine();
//            }
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        }


//        OVERWRITING A TEXT FILE / FILE'S TEXT ...?

//        create a string variable
//        String overwriteText = "Listen to many, speak to a few. ";
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./movieQuotes/hamilton.txt")))
//        {
//            writer.write(overwriteText);
//            writer.newLine();
//        }
//        catch (IOException e) {
//            System.out.println(e.getMessage());
//        }


//      RENAMING A TEXT FILE
//        File oldFileName = new File("./movieQuotes/hamilton.txt");  //targeting the file we want to rename
//        File newFileName = new File("./moviesQuotes/shakespeareHamilton");  //creating the name that we want

//        oldFileName.renameTo(newFileName);

//        DELETING A TEXT FILE...
//     File newFileName = new File("./moviesQuotes/shakespeareHamilton");  //creating the name that we want
//        newFileName.delete();  //cli 'rm - rf file name






/*
        *******************************************************************
 */
//        Create a new directory OUTSIDE of our src directory, and name it 'albums'.
//        Taking the example from our File Handling lecture,
//        add three albums our your choice into your albums directory.
//        When READING from a text file, the terminal output should look as such:

//        Album: Darkside of the Moon
//        =============
//        Artist: Pink Floyd
//        Year: 1978
//        Genre: Rock
//        ==============


    String line;

    BufferedReader bufferedReader = null;

    try {
        bufferedReader = new BufferedReader(new FileReader("./albums/DarkSideOfTheMoon.txt"));
        line = bufferedReader.readLine();
        while (line != null) {
            System.out.println(line);
            line = bufferedReader.readLine();
        }
    }   catch (IOException e) {
            System.out.println(e.getMessage());
    }   finally {
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        String text = "Album: Darkside of the Moon\n" +
                        "============= \n" +
                        "Artist: Pink Floyd\n" +
                        "Year: 1978\n" +
                        "Genre: Rock \n" +
                        "============= ";
        String text2 = "Album: Darkside of the Moon\n" +
                        "============= \n" +
                        "Artist: Pink Floyd\n" +
                        "Year: 1978\n" +
                        "Genre: Rock \n" +
                        "============= ";
        String text3 = "Album: Darkside of the Moon\n" +
                        "============= \n" +
                        "Artist: Pink Floyd\n" +
                        "Year: 1978\n" +
                        "Genre: Rock \n" +
                        "============= ";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./albums/DarkSideOfTheMoon.txt", true))) {
            writer.write(text);
            writer.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }


    }

    }//end of main method
}//end of filehandling class

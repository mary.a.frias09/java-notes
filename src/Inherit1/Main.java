package Inherit1;
import java.util.ArrayList;
import java.util.List;

public class Main {


    public static void main(String[] args) {
        //declaration of the object variable a1 of the animal class
        //EXAMPLE OF POLYMORPHISM
        Animal a1;

        //object creation of the animal class
        a1 = new Animal();
        a1.displayInfo();
        //object creation of the dog class
        a1 = new Dog();
        a1.displayInfo();
        //object creation of the cat class
        a1 = new Cat();
        a1.displayInfo();

    }







}

//
//    public static void main(String[] args) {
//        //declaring dog
//        Dog dog1 = new Dog();//creating dog object
//
//        dog1.eat();
//        dog1.displayInfo("Red","Labrador", 2);
//
//
//
//
//
//    }
//}


package SchoolInherit;

public class Main {

    public static void main(String[] args) {
        PhysicsTeacher obj = new PhysicsTeacher();  //creating object of PhysicsTeacher
        System.out.println(obj.collegeName);
        System.out.println(obj.designation);
        System.out.println(obj.mainSubject);

        obj.does();
    }
}

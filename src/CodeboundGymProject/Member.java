package CodeboundGymProject;

public class Member {
//    protected Object club;
//
    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    private String memberName;

    public double getMemberFee() {
        return memberFee;
    }

    public void setMemberFee(double memberFee) {
        this.memberFee = memberFee;
    }

    private double memberFee;

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    private String memberType;

    public double getMemberId() {
        return memberId;
    }

    public void setMemberId(double memberId) {
        this.memberId = memberId;
    }

    private double memberId;

    @Override
    public String toString () {

        return "Member{" +
                "memberType='" + memberType + '\'' +
                ", memberId=" + memberId +
                ", name='" + memberName + '\'' +
                ", fees=" + memberFee +
                '}';
    }

    public Member(String name, double fee, String memberType, double fees, double memberId) {
        memberName = name;
        memberFee = fee;
        this.memberType = memberType;
        this.memberId = memberId;
        System.out.println();
    }


}//end of member class

// This class contains the basic information about each member.

// It serves as a parent class from which other classes will be derived.
//  Fields
//  This class has four private fields:
//  •memberType
//  •memberID
//  •name
//  •fees
//
// Constructor
// Create the constructor for the Member class.
// This class has one constructor with four parameters.
// These parameters are going to be the fields you created.
// Inside of the constructor, we assign the four parameters to the appropriate fields.
// *Remember the shortcut for creating a constructor?

// Methods
// Now let’s create the setter and getter methods for the four private fields above.
// All setter and getter methods are public.

// Each setter method has an appropriate parameter and assigns the parameter to the field.
// Each getter method returns the value of the field.
// *Remember the shortcuts to create the setter and getter methods?
//
// Finally, let’s write a toString() method for the class.
// NOTE: The toString() method is a pre-written method in the Object class that returns
// a string representing the object.
//
// This method is going to be public but also overridden. The only thing this method
// returns is a string that provides information about a particular member.
// For example, the method returns a string with the following information:
//
// “S, 1, Stephen, 950.00”

// Where S, 1, Stephen, 950.00 are the values of memberType, memberID, name, and fees
// respectfully.


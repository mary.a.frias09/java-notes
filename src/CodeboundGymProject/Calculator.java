package CodeboundGymProject;

public interface Calculator < T extends Number > {


      public abstract double calculatorFees(T clubID);


}//end of calculator interface






//      *** The Calculator Interface ***
//  This interface is a generic interface.
//
//  Create a Java Interface named Calculator.
//  This interface only works with numerical data types.
//  Hence, it accepts a bounded type parameter.
//
// Your declaration should look like the following:
//
//public interface Calculator<T extends Number>
//  After declaring the interface, add a method to it.
//        •	This method is an abstract method named calculateFees()
//        •	Also, this method takes in one parameter named clubID
//        •	And returns a double value

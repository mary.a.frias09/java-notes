package CodeboundGymProject;

import java.util.LinkedList;
import java.util.Scanner;

public class MembershipManagement {

// First, declare a Scanner object inside of the class, and use it to read the user’s input.
// Let’s call this object ‘userInput’, and declare it as final and private
//*We are declaring userInput as final because we will not be assigning any new reference to it later in our code. We also declare it as private because we’ll only be using it in our MembershipManagement class.

 Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
//        private void userInput();{

//        }
    }
// The first private methods will be called ‘getIntInput()’
// •This method is called whenever any method in the MembershipManagement class prompts the user to enter an int value.
// •The method tries to read the entered int value.
// •If the user fails to enter an int value, the method keeps prompting the user to enter a new value until it gets a valid input from the user
//	•This method does not have any parameters
//	•It returns an int value.

    private int getIntInput(){
        return 0;
    }

//The second private method will be called ‘printClubOptions()’
// •This method has no parameters
//•	Does not return any value
//•	This method simply prints out the following text:
//•	Club Alpha
//•	Club Bravo
//•	Club Charlie
//•	Multi Clubs
        private void printClubOptions(){
            System.out.println("Club Alpha \n " +
                               "Club Bravo \n " +
                               "Club Charlie \n " +
                               "Multi Clubs ");
        }

//  method will be called getChoice()
//	•	It has no parameters
//	•	Returns an int value
//	•	Has a local int variable
//	•	Prints out the following text:
//    WELCOME TO ALPHA FITNESS CENTER
//    =============================
//    Please select an option (or Enter -1 to quit):
//    1) Add Member
//    2) Remove Member
//    3) Display Member Information
//    =============================
    public int getChoice() {
        int userSelection;
        System.out.println(" WELCOME TO ALPHA FITNESS CENTER\n" +
                           "    =============================\n" +
                           "    Please select an option (or Enter -1 to quit):\n" +
                           "    1) Add Member\n" +
                           "    2) Remove Member\n" +
                           "    3) Display Member Information\n" +
                           "    =============================");
      userSelection = getIntInput();
      return userSelection;
    }


//    The method is declared as the following:
//    public String addMembers(LinkedList<Member> m) {
////    }
//	•	This method consists of 7 local variables
//    String name;
//    int club;
//    String mem;
//    double fees;
//    int memberID;
//    Member mbr;.....

public void addMembers(LinkedList<Member> m) {
    String name;
    int club;
    String mem;
    double fees;
    int memberID;
    Member mbr;
    Calculator<Integer> cal;
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please enter the member's name: ");
    name = scanner.nextLine();
    printClubOptions();
    System.out.println("Enter a club ID: ");
    club = scanner.nextInt();
    while (club < 1 || club > 4) {
        System.out.println("Please enter a valid Club ID (1-4)");
        club = getIntInput();
    }
    //Assign member ID
    if (m.size() > 0)
//        memberID = m.getLast().getMemberID() + 1;
//    else
        memberID = 1;
    if (club != 4) {
        cal = (n) -> {
            switch (n) {
                case 1:
                    return 900;
                case 2:
                    return 950;
                case 3:
                    return 1000;
                default:
                    return -1;
            }
        };
//        Add member to LinkedList
//        fees = cal.calculateFees(club);
//        mbr = new SingleClubMember('S', memberID, name, fees, club);
//        m.add(mbr);
//        mem = mbr.toString();
//        System.out.println("Single member has been added!");
    } else {
        cal = (n) -> {
            if (n == 4) {
                return 1200;
            } else {
                return -1;
            }
        };
//        fees = cal.calculateFees(club);
//        mbr = new MultiClubMember('M', memberID, name, fees, 100);
//        m.add(mbr);
//        mem = mbr.toString();
        System.out.println("Multi member has been added!");
    }
    //    return mem;
} // End of addMember Method


   //    Moving onto the third public method.
//    This method does not return anything.
//    It takes in a LinkedList of Member objects and removes a member from this LinkedList.
//            Name this ‘LinkedList m’
//    In this method, declare a local int variable for the member ID.
//            Next, prompt the user to enter the Member ID of the member that he/she wants to remove.
//    Use the getIntInput() method to read the input and assign it to your variable.
//    Use the following for statement to loop through the LinkedList:
//    for ( int i = 0; i < m.size(); I++) {
//    }
//    Within the for statement, use the if statement to compare the memberID of each element with the    member ID that the user entered.
//    if (m.get(I).getMemberID() == memberID) {
//    }
//    Within this if statement, three things will happen:
//            - Use the remove() method to remove the element at index i from the LinkedList
//    - Prompt the user “Member Removed”
//            - Use the return statement to exit the method
//        - return;
//    Outside of our for statement, prompt the user that “Member ID not found”
//            - If the user enters an ID that doesn’t exist, they receive this message.

    public void removeMember(LinkedList<Member> members) {

    }



//    This method prompts the user to “Enter Member ID to display information: “
//    Based on the user’s input, the information of the member will be displayed.
//    If the member is a single club member, the information displayed should look like:
//    Member Type = S
//    Member ID = 1
//    Member Name = Stephen
//    Membership Fees = 950.0
//    Club ID = 2
//
//    If the member is a multi club member, the information displayed should look like:
//    Member Type = M
//    Member ID = 2
//    Member Name = Karen
//    Membership Fees = 1200.0
//    Membership Points = 100
    public void printMemberInfo(LinkedList<Member> members) {

    }
}//end of  class

















//*** The MembershipManagement Class ***
//  This class is the main focus of the program. It handles the process of adding and removing member. It also has a method that allows users to display information about a member.
//
//  First, declare a Scanner object inside of the class, and use it to read the user’s input.
//  Let’s call this object ‘userInput’, and declare it as final and private
//*We are declaring userInput as final because we will not be assigning any new reference to it later in our code. We also declare it as private because we’ll only be using it in our MembershipManagement class.
//
//  Methods
//   Next, let’s create two private methods.
//  Declare them both as private because these methods are only needed in this class.
//
// The first private methods will be called ‘getIntInput()’
//  • This method is called whenever any method in the MembershipManagement class prompts the user to enter an int value.
//  •	The method tries to read the entered int value.
//  •	If the user fails to enter an int value, the method keeps prompting the user to enter a new value until it gets a valid input from the user
//   •	This method does not have any parameters
//  •	It returns an int value.
//
// The second private method will be called ‘printClubOptions()’
//  •	This method has no parameters
//  •	Does not return any value
//   •	This method simply prints out the following text:
//   •	Club Alpha
//   •	Club Bravo
//   •	Club Charlie
//    •	Multi Clubs
//
//  Next, create four more methods for this class.
//  All four methods are public.
//
//
//
//
//
//        First method will be called getChoice()
//        •	It has no parameters
//        •	Returns an int value
//        •	Has a local int variable
//        •	Prints out the following text:
//
//        WELCOME TO ALPHA FITNESS CENTER
//        =============================
//        Please select an option (or Enter -1 to quit):
//        1) Add Member
//        2) Remove Member
//        3) Display Member Information
//        =============================
//
//        •	It calls the getIntInput() method to read the user’s input and assigns the user input to the local int variable.
//
//        addMembers()
//        •	Takes in a LinkedList of Member objects and adds a new member to this LinkedList
//        •	Then returns a string that contains information about the member added.
//
//        The method is declared as the following:
//
//public String addMembers(LinkedList<Member> m) {
//        }
//
//        •	This method consists of 7 local variables
//        String name;
//        int club;
//        String mem;
//        double fees;
//        int memberID;
//        Member mbr;
//        Calculator<Integer> cal;
//        *Note the last variable is a reference to the Calculator interface that we coded earlier.
//
//        Getting the member’s name
//        •	Prompt the user to “Please enter the member’s name: “
//        •	Assign the name variable to your userInput to read the input
//
//        Getting the member’s club access
//        •	First call the printClubOptions() to prompt the user to enter the club ID that the member has access to.
//        •	Next, assign the club variable to your userInput to read the input.
//        •	Validation: The valid values for a club are 1 – 4
//
//        Calculating the member ID
//        Now let’s calculate the member ID for the new member.
//
//        The member ID is an auto-incremented number that is assigned to each new member
//        In other words, if the previous member has an ID of 10, the new member will have an ID of 11.
//        Here is one way of calculating the member ID:
//
//        if (m.size() > 0)
//        memberID = m.getLast().getMemberID() + 1;
//        else
//        memberID = 1;
//
//        Adding the member to the LinkedList
//        Now that we have gotten the member’s name, club ID and member ID, we are ready to add the member to the LinkedList m
//        Remember, we add a member based on the variable ‘club’.
//        If the value of club is 1, 2, or 3, the member is a single club member.
//        If the value is 4, the member is a multi club member
//        *Think about which ‘tool’ you’ll use for this…
//
//        Adding a Single Club Member
//        Now let’s look at how we’ll add a single club member.
//        We need to first calculate the membership fee of a single club member.
//        The club IDs and fees for each club are shown below:
//        Club Mercury
//        ID = 1, Fees = 900
//        Club Neptune
//        ID = 2, Fees = 950
//        Club Jupiter
//        ID = 3, Fees = 1000
//
//        The following code shows the implementation of the calculateFees() method for a single club member:
//        cal = (n) -> {
//        switch (n) {
//        case 1:
//        return 900;
//        case 2:
//        return 950;
//        case 3:
//        return 1000;
//default:
//        return -1;
//        }
//        };
//
//        Here, we use a switch statement to implement the method.
//        This is statement is in a what we call a lambda expression.
//
//        Once this statement is complete, we will then use the following code to calculate the membership fee of the single club member and assign it to the variable fees:
//        fees = cal.calculateFees(club);
//
//        Next, we’ll instantiate a new SingleClubMember object by passing in the char value ’S’, and the variables memberID, name, fees, and club to the SingleClubMember constructor.
//        We’ll assign this object to the local ‘Member’ variable mbr from above.
//
//        After this, we use the add() method to add mbr to the LinkedList m
//        After adding the new member, we generate a string representing the new member and assign it to the local variable mem (again from above)
//        •	To do this, we simply use mbr to call the toString() method in the SingleClubMember class
//
//Finally, we use the statement below to inform the user that the member is added successfully.
//        “STATUS: Single Club Member added”
//
//        Adding a Multi Club Member
//        Taking what we came up to add a single club member, try to come up with the logic and code to add a Multi Mlub Member.
//
//        removeMember() method
//        Moving onto the third public method.
//        This method does not return anything.
//        It takes in a LinkedList of Member objects and removes a member from this LinkedList.
//        Name this ‘LinkedList m’
//
//        In this method, declare a local int variable for the member ID.
//        Next, prompt the user to enter the Member ID of the member that he/she wants to remove.
//        Use the getIntInput() method to read the input and assign it to your variable.
//
//
//
//
//
//
//
//        Use the following for statement to loop through the LinkedList:
//
//        for ( int i = 0; i < m.size(); I++) {
//        }
//
//        Within the for statement, use the if statement to compare the memberID of each element with the member ID that the user entered.
//
//        if (m.get(I).getMemberID() == memberID) {
//        }
//
//        Within this if statement, three things will happen:
//        - Use the remove() method to remove the element at index i from the LinkedList
//        - Prompt the user “Member Removed”
//        - Use the return statement to exit the method
//        - return;
//
//        Outside of our for statement, prompt the user that “Member ID not found”
//        - If the user enters an ID that doesn’t exist, they receive this message.
//
//
//        printMemberInfo()
//        Similar to the removeMember() method, it also takes in a LinkedList of Member objects as a parameter, and does not return anything.
//
//        This method prompts the user to “Enter Member ID to display information: “
//        Based on the user’s input, the information of the member will be displayed.
//
//        If the member is a single club member, the information displayed should look like:
//
//        Member Type = S
//        Member ID = 1
//        Member Name = Stephen
//        Membership Fees = 950.0
//        Club ID = 2
//
//        If the member is a multi club member, the information displayed should look like:
//
//        Member Type = M
//        Member ID = 2
//        Member Name = Karen
//        Membership Fees = 1200.0
//        Membership Points = 100

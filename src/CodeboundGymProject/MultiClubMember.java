package CodeboundGymProject;

public class MultiClubMember extends Member{

    public int getMembershipPoints() {
        return membershipPoints;
    }

    public void setMembershipPoints(int membershipPoints) {
        this.membershipPoints = membershipPoints;
    }

    private int membershipPoints;

    public MultiClubMember(String name, double fee, String memberType, double fees, double memberId, int membershipPoints)
    {
        super(name, fee, memberType, fees, memberId);
        this.membershipPoints = membershipPoints;
    }

    @Override
    public String toString() {
        return "MultiClubMember{" +"memberType='" + getMemberType() + '\'' +
                ", memberId=" + getMemberId() +
                ", name='" + getMemberName() + '\'' +
                ", fees=" + getMemberFee() + '\'' +
                "membershipPoints=" + membershipPoints +
                '}';
    }
//    public static void main(String[] args) {
//    }//end of main
}//end of class


// This class is also going to be a subclass for the Member class.
// The MultiClubMember class has one private int field called ‘membershipPoints’.
//
// Constructor
// This class’ constructor is going to look very similar to the SingleClubMember’s
// constructor.
// This constructor also has five parameters, which are the fields being
// inherited from the
// Member class along with the field you just created from this class.
//
// Methods
// Add the getter and setter methods for the membershipPoints field only.
//
// Finally, create the toString() method for this class as well.
// This method is public, similar to the toString() method in the parent class,
// but displays
// an additional piece of information – the club that the member belongs to.
// This method prints out the following:
//
// “M, 2, Karen, 1320.00, 100”
//
// Where M, 2, Karen, 1320.00, and 100 are the values of the memberType,
// memberID, name, fees, and membershipPoints fields.
//
// You are now done with the Member class and its subclasses.

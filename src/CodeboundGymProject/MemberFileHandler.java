package CodeboundGymProject;
import java.io.BufferedReader;
import java.util.LinkedList;

public class MemberFileHandler {

//    private Object LinkedList;

    public LinkedList<Member> readFile() {
       LinkedList <Member> m = new LinkedList<>() {
       String lineRead;
//       String[] splitline;
       Member mem;
       BufferedReader reader = null;
       };
        return null;
    }



//    appendFile()
//    This method appends a new line to the members.csv file whenever a new member is added.
//    This method has one parameter - a String parameter called ‘mem’
//    This method does not return anything
//
//    Within this method, create a BufferedWriter object and name it ‘writer’.
//    Pass in a FileWriter object to the BufferedWriter constructor
//*Remember we are WRITING TO A FILE
//**This can be found in our FileHandling lesson

    public void appendFile(String mem) {

    }

//    overwriteFile()
//    This method has a LinkedList<Member> parameter named ‘m’, and does not return anything.
//    This method is called whenever we want to remove a member from the club.
//    When we remove a member, we need to update our csv file.
//            However, there’s no method in Java that allows us to easily remove a line from a file (this is beyond the scope of this exercise)
//    We can only write or append to it.
//    For this project, we’ll need to create a temporary file instead.
//    This is a common practice in programming.
//            First, we need to declare a local variable in this method.
//
//- Create a String variable named ’s’
//    Next, let’s use a try-catch statement to create a BufferedWriter object called writer
//
//- Pass in the following FileWriter  object to its constructor:
//            new FileWriter(“members.temp”, false)
//
//- After creating the BufferedWriter object, include the following inside of your try block:
//            for (int i=0; i< m.size(); i++)
//    {
//        s = m.get(i).toString();
//        writer.write(s + "\n");
//    }
//- For your catch, pass in the IOException exception as your parameter
//- Inside of your catch, sout your exception with the getMessage();
//
//    Almost there!
//    What is left? We need to delete the original members.csv file and rename members.temp to members.csv

    public void overwriteFile(LinkedList<Member> members) {
    }


}//end of class
//      *** The MemberFileHandler Class ***
// This class consists of three public methods:
//  readFile()
//  appendFile()
//  overWriteFile()
// *NOTE: Just in case they don’t generate automatically, make sure that you have
// ‘import java.util.LinkedList’, and ‘import java.io.*’

// Methods
// readFile()
//  This public method shouldn’t have any parameters, but should return a LinkedList of Member objects.
//  Next, let’s implement the method.

//  The readFile() method reads from a csv file that contains the details of each member.
// It then adds each member to a LinkedList and returns the LinkedList.
// The format of the txt file is:

//        For Single Club Members
//        Member Type, Member ID, Member Name, Membership Fees, Club ID

//        For Multi Club Members
//        Member Type, Member ID, Member Name, Membership Fees, Membership Points

//        Examples
//        S, 1, Stephen, 950.00, 2
//        M, 2, Karen, 1200.00, 100
//        Name the text file is members.csv and is stored in the same folder as the project
//        (when you run the application, it should auto-generate in your file structure)
//        In this class we need to declare four local variables:
//        LinkedList<Member> m = new LinkedList();
//        String lineRead;
//        String[] splitLine;
//        Member mem;

//        After declaring the variables, we’ll use a try-with-resources statement to create a
//        BufferedReader object.

//        Let’s name the BufferedReader object ‘reader’
//        - reader accepts the FileReader object that reads from members.csv
//        Your try-with-resources statement should look like the following:
//        try (BufferedReader reader = new BufferedReader(new FileReader(“members.csv”)))
//        {
//        // code inside try block
//        }
//        catch (IO Exception e)
//        {
//        // code inside catch block
//        }

//        Inside our try block
//        - Create a local String variable named lineRead
//        - Assign lineRead to reader.readLine()
//        - Next, create a while statement to process the file line by line while lineRead is not null
//        - Within the while statement, copy the following code:


//        splitLine = lineRead.split(", ");
//        if (splitLine[0].equals("S"))
//        {
//        mem = new SingleClubMember('S', Integer.parseInt(splitLine[1]), splitLine[2],
//        Double.parseDouble(splitLine[3]), Integer.parseInt(splitLine[4]));
//        }else
//        {
//        mem = new MultiClubMember('M', Integer.parseInt(splitLine[1]), splitLine[2],
//        Double.parseDouble(splitLine[3]), Integer.parseInt(splitLine[4]));
//        }
//        m.add(mem);
//        lineRead = reader.readLine();

//        While statement and Try block completed

//        For our catch block
//        - Pass in the IOException as your argument
//        - Within the catch block, sout the exception with the getMessage() method

//        Catch block completed

//        For your “missing return statement”, return your local variable ‘m’ from above

//        appendFile()
//        This method appends a new line to the members.csv file whenever a new member is added.
//        This method has one parameter - a String parameter called ‘mem’
//        This method does not return anything

//        Within this method, create a BufferedWriter object and name it ‘writer’.
//        Pass in a FileWriter object to the BufferedWriter constructor
//        *Remember we are WRITING TO A FILE
//        **This can be found in our FileHandling lesson

//        overwriteFile()
//        This method has a LinkedList<Member> parameter named ‘m’, and does not return anything.
//        This method is called whenever we want to remove a member from the club.
//        When we remove a member, we need to update our csv file.
//        However, there’s no method in Java that allows us to easily remove a line from a file
//        (this is beyond the scope of this exercise)
//        We can only write or append to it.
//        For this project, we’ll need to create a temporary file instead.
//        This is a common practice in programming.
//        First, we need to declare a local variable in this method.

//        - Create a String variable named ’s’
//        Next, let’s use a try-catch statement to create a BufferedWriter object called writer

//        - Pass in the following FileWriter  object to its constructor:

//        - After creating the BufferedWriter object, include the following inside of your try block:
//        for (int i=0; i< m.size(); i++)
//        {
//        s = m.get(i).toString();
//        writer.write(s + "\n");
//        }
//        - For your catch, pass in the IOException exception as your parameter
//        - Inside of your catch, sout your exception with the getMessage();

//        Almost there!
//        What is left? We need to delete the original members.csv file and rename members.temp to members.csv

package CodeboundGymProject;

public class SingleClubMember extends Member {

    private int club;
    public SingleClubMember(String memberType, Integer memberID, String name, double fees, int club)
    {
        super(memberType, memberID, name, fees, club);
        this.club = club;
    }
    public int getClub() {
        return club;
    }
    public void setClub(int club) {
        this.club = club;
    }
    @Override
    public String toString() {
        return super.toString();
    }
} // End of class

//      *** The SingleClubMember Class ***
// This class is a subclass for the Member class.
//
// Fields
// The SingleClubMember class has one private Integer field name ‘club’.

// Constructor
// This class has one constructor with five parameters, which are the fields being inherited from the
// Member class, along with the field you just created for this class.

// Methods
// Add the getter/setter methods for the club field only.

// Finally, create the toString() method for this class as well.
// This method is public, similar to the toString() method in the parent class, but displays an additional
// piece of information – the club that member belongs to.
// For example, the method returns a string with the following information:

// “S, 1, Faith, 950.00, 2”

// Where S, 1, Faith, 950.00, and 2 are values of the memberType, memberID, name, fees, and club fields
// respectfully.

























//package CodeboundGymProject;
//
//public class SingleClubMember extends Member {
//    private int club;
//
//    public SingleClubMember(String name, double fee, String memberType, double memberId) {
//        super(name, fee, memberType, memberId);
//    }
//
//    public int getClub() {
//        return (int) club;
//    }
//
//    public void setClub(int club) {
//        this.club = club;
//    }
//
//    @Override
//    public String toString() {
//        return super.toString();
//    }
//}
//    }
//}//end of singleclubmember class

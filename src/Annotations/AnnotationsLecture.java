package Annotations;

import Collections.List;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class AnnotationsLecture {
//    Annotations-
//            - used to provide metadata for java code,
//- Don’t modify the execution of our code, they can be consumed in order to perform additional logic

//    Predefined Annotations
//    @Override

//public void displayInfo

    public class Animal extends AnnotationsLecture {
        public void displayInfo() {
            System.out.println("I am an animal");
        }
    }

    public class Dog extends AnnotationsLecture {
//        @Override
        public void displayInfo() {
            System.out.println("I am a dog");
        }
    }

// @SuppressWarnings - annotation makes the compiler suppress warnings for a given method.

//    @SuppressWarnings("unchecked")
//    public List<String> languages(){
//    return new ArrayList();
//    }

// The method is defined as returning a List <String>, but the actual return value is just a generic ArrayList.
    // because of this, a warning about unchecked types will occur when the code is compiled.

    //@Deprecated- is used to indicate that a class, method, or a fields should no longer be used.

    //@Deprecated
    //private String firstName;

    //@Deprecated
    //public class MyComponent {
    // }







}//end of annotations class
